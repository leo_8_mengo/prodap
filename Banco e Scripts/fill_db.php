<?php
function db_categorias() {
  $entrada = fopen("./categorias.txt","r");    
  $conexao = pg_connect("host=localhost port=5432 dbname=PRODAP user=postgres password=postgres") or die("conexao falhou");

  $result = pg_query($conexao, "DELETE FROM \"Dicionario\".\"Categorias\";");
  while (!feof($entrada)) {
    $linha = fgets($entrada, 4096);

    if (!feof($entrada)) {
      $query = "INSERT INTO \"Dicionario\".\"Categorias\" (nome) VALUES ('".trim($linha)."');";

      $result = pg_query($conexao, $query);
      if (!$result) {
        echo("Query failed: ".trim($linha)."<br>");
      } else {
        echo("Inserted: ".trim($linha)."<br>");
      }
    }
  }
  echo("<br>");

  pg_close($conexao);
  fclose($entrada);
}

function db_bairros() {
  $entrada = fopen("./bairros.txt", "r");

  $conexao = pg_connect("host=localhost port=5432 dbname=PRODAP user=postgres password=postgres") or die("conexao falhou");

  $result = pg_query($conexao, "DELETE FROM \"Dicionario\".\"Palavras\";");
  while (!feof($entrada)) {
    $linha = fgets($entrada, 4096);
    $pieces = explode(">>", $linha);

    if (!feof($entrada)) {
      $query = "INSERT INTO \"Dicionario\".\"Palavras\" (nome, link) VALUES ('%s', '%s');";
      $query = sprintf($query, trim($pieces[0]), trim($pieces[1]));

      $result = pg_query($conexao, $query);
      if (!$result) {
        echo("Query failed: ".trim($pieces[0]).", ".trim($pieces[1])."<br>");
      } else {
        echo("Inserted: ".trim($pieces[0]).", ".trim($pieces[1])."<br>");
      }
    }
  }
  echo("<br>");

  pg_close($conexao);
  fclose($entrada);
}

db_categorias();
db_bairros();
?>
