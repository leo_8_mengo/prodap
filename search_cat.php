<?php
$conexao = pg_connect("host=localhost port=5432 dbname=PRODAP user=postgres password=123") or die("Erro ao conectar com o banco de dados");

$q = $_REQUEST["q"];

$link = [];
$foto = [];
$response = [];
$palavras = [];

function invalid() {
    echo json_encode(array("code" => 404, "res" => array("Tabela inválida")), JSON_UNESCAPED_UNICODE);
    exit(1);
}

$result = @pg_query($conexao, "SELECT * FROM \"Dicionario\".\"".$q."\" ORDER BY nome;") or invalid();

while ($row = pg_fetch_assoc($result)) {
    $response["code"] = 200;
    $response["res"][] = array($row['nome'], $row['link'], $row['foto'], $row['def'], $row['id']);
}

echo json_encode($response, JSON_UNESCAPED_UNICODE);
pg_close($conexao);
?>
